#
#  Makefile for Go
#

NAME:=saferwatcher
PWD:=$(shell pwd)
TMP:=/tmp/.go_project/src/github.com/safervpn
OS:=$(shell uname)
VERSION:=$(shell git describe --tags --always)
PKGS:=./cmd/... ./pkg/...

export PATH:=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/go/bin:/go/bin
export VAULT_ADDR:=http://127.0.0.1:8200
export VAULT_TOKEN:=c246e9ee-481f-0f41-b31f-724f8efbbdea

.PHONY: init
init: ## Install all dependencies and configure env.
	# Cleaning before setup
	@$(MAKE) clean

	# Update vendors
	@$(MAKE) dep

	# Setup dependencies
	@$(MAKE) env

.PHONY: dep
dep: ## Update vendors
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && GOPATH=/tmp/.go_project dep ensure

	rm -f ${TMP}/${NAME}
else
	dep ensure
endif

.PHONY: env
env: ## Configuring env for application.
	# Up all docker containers for project
	docker-compose up -d

.PHONY: test
test: ## Run all the tests
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && GOPATH=/tmp/.go_project go test -cover -covermode=atomic -v -race -timeout=10m $(PKGS)

	rm -f ${TMP}/${NAME}
else
	go test -cover -covermode=atomic -v -race -timeout=30s $(PKGS)
endif

.PHONY: fmt
fmt: ## Run goimports on all go files
	find . -name '*.go' -not -wholename './vendor/*' | while read -r file; do goimports -w "$$file"; done
	go fmt $(PKGS)

.PHONY: lint
lint: ## Run all the linters
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && \
	GOPATH=/tmp/.go_project gometalinter --vendor --disable-all \
		--enable=deadcode \
		--enable=ineffassign \
		--enable=gosimple \
		--enable=staticcheck \
		--enable=gofmt \
		--enable=goimports \
		--enable=misspell \
		--enable=errcheck \
		--enable=vet \
		--enable=vetshadow \
		--deadline=10m \
		$(PKGS)

	rm -f ${TMP}/${NAME}
else
	gometalinter.v2 --disable gas --vendor \
		--enable=deadcode \
		--enable=ineffassign \
		--enable=gosimple \
		--enable=staticcheck \
		--enable=gofmt \
		--enable=goimports \
		--enable=misspell \
		--enable=errcheck \
		--enable=vet \
		--enable=vetshadow \
		--deadline=10m \
		$(PKGS)
endif

.PHONY: ci
ci: lint test ## Run all the tests and code checks

.PHONY: build
build: ## Build a version
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && \
	GOPATH=/tmp/.go_project go build -v -ldflags="-X main.Version=${VERSION}" -o usr/local/sbin/${NAME} ./cmd/saferwatcher/main.go

	rm -f ${TMP}/${NAME}
else
	go build -v -ldflags="-X main.Version=${VERSION}" -o bin/${NAME} ./cmd/saferwatcher/main.go
endif

.PHONY: build-deb
build-deb: ## Build deb package
	dpkg-deb --build ./ ${NAME}.deb

.PHONY: install
install: ## Installing SaferX Backend
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && \
	cp -a ./bin/${NAME} /usr/local/sbin/saferwatcher

	rm -f ${TMP}/${NAME}
else
	echo "not implemented"
endif

.PHONY: clean
clean: ## Remove temporary files
ifeq ($(origin GOPATH), undefined)
	mkdir -p ${TMP}
	ln -sf ${PWD} ${TMP}/${NAME}

	cd ${TMP}/${NAME} && GOPATH=/tmp/.go_project go clean

	rm -f ${TMP}/${NAME}
else
	go clean
endif

	docker-compose down

# All
.PHONY: all
all: ## Run all targets
	@$(MAKE) init ci build install clean

# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# Default make
default: init test build clean

.DEFAULT_GOAL := default
