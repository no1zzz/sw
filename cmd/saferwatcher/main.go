package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"runtime/pprof"
	"syscall"
	"time"

	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/api"
	"github.com/safervpn/saferwatcher/pkg/backends/influxdb"
	"github.com/safervpn/saferwatcher/pkg/backends/redis"
	"github.com/safervpn/saferwatcher/pkg/config"
	"github.com/safervpn/saferwatcher/pkg/model"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/cpu"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/hdd"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/la"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/mem"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/uptime"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/ikev2"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/l2tp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/openvpn"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/pptp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/proxy"
	"github.com/safervpn/saferwatcher/pkg/worker"
)

var (
	log *logger.Log // Using log subsystem
	p   = make(map[string]plugins.HealthChecker)
)

const pollInterval = 15 * time.Second

func init() {
	var (
		err       error
		statsFile string
		procName  string
	)

	// Parse flags
	cfg := flag.String("config", "config.yml", "Path to yaml config file")
	flag.Parse()

	// Build config
	if err = config.Build(*cfg); err != nil {
		panic(err)
	}

	// Initialization log system
	cfgDaemon := config.Daemon()
	if log, err = logger.NewFileLogger(cfgDaemon.LogFile(), cfgDaemon.LogLevel()); err != nil {
		panic(err)
	}

	//Initialization in-mem db
	if err = model.InitInMemDB(); err != nil {
		log.Critf("Can't init in-memory db: %v", err)
	}

	// Initialization InfluxDB connection
	if cfgDaemon.InfluxDBEnabled() {
		if err = influxdb.Connect(
			cfgDaemon.InfluxDBHost(), cfgDaemon.InfluxDBLogin(), cfgDaemon.InfluxDBPass(),
		); err != nil {
			log.Critf("Can't connect to InfluxDB: %v", err)
		}

		if err = model.InitInfluxDB(cfgDaemon.InfluxDBName(), cfgDaemon.MaxQueue(), pollInterval); err != nil {
			log.Critf("Can't init in-memory db: %v", err)
		}
	}

	// Load CPU plugin
	cfgPlugCPU := config.SystemPlugins().CPU
	if cfgPlugCPU.Enabled() {
		p[cpu.NAME] = cpu.NewHealthCheck(pkg.Attribute{cpu.ParamProcStat: cfgPlugCPU.ProcStatFilePath()})
	}

	// Load LA plugin
	cfgPlugLA := config.SystemPlugins().LA
	if cfgPlugLA.Enabled() {
		p[la.NAME] = la.NewHealthCheck(nil)
	}

	// Load Uptime plugin
	cfgPlugUptime := config.SystemPlugins().Uptime
	if cfgPlugUptime.Enabled() {
		p[uptime.NAME] = uptime.NewHealthCheck(nil)
	}

	// Load Mem plugin
	cfgPlugMem := config.SystemPlugins().Mem
	if cfgPlugMem.Enabled() {
		p[mem.NAME] = mem.NewHealthCheck(nil)
	}
	//// Load Hdd plugin
	cfgPlugHdd := config.SystemPlugins().Hdd
	if cfgPlugHdd.Enabled() {
		p[hdd.NAME] = hdd.NewHealthCheck(nil)
	}

	// Load IKEv2 plugin
	cfgPlugIKEv2 := config.VPNPlugins().IKE2
	if cfgPlugIKEv2.Enabled() {
		p[ikev2.NAME] = ikev2.NewHealthCheck(nil)
	}

	// Load l2tp plugin
	cfgPlugL2TP := config.VPNPlugins().L2TP
	if cfgPlugL2TP.Enabled() {
		p[l2tp.NAME] = l2tp.NewHealthCheck(nil)
	}

	// Load ovpn plugin
	cfgPlugOVPN := config.VPNPlugins().OVPN
	for k, v := range cfgPlugOVPN {
		if v.Enabled() {
			statsFile, err = v.StatFile()
			if err != nil {
				log.Critf("can't init ovpn plugin: %v", err)
			}
			procName, err = v.ProcName()
			if err != nil {
				log.Critf("can't init ovpn plugin: %v", err)
			}
			p[fmt.Sprintf("%s-%s", openvpn.NAME, k)] = openvpn.NewHealthCheck(
				pkg.Attribute{openvpn.ParamStatsFile: statsFile, openvpn.ParamProcName: procName},
			)
		}
	}

	// Load l2tp plugin
	cfgPlugPPTP := config.VPNPlugins().PPTP
	if cfgPlugPPTP.Enabled() {
		p[pptp.NAME] = pptp.NewHealthCheck(nil)
	}
	//Load proxy plugin
	cfgPlugPROXY := config.VPNPlugins().PROXY
	if cfgPlugPROXY.Enabled() {
		//Connecting to redis
		if err = redis.Connect("localhost:6379"); err != nil {
			log.Critf("Can't init redis db: %v", err)
		}

		p[proxy.NAME] = proxy.NewHealthCheck(nil)
	}
}

// Main initialize core (configs, logger) and run daemon
func main() {
	var (
		cfg = config.Daemon()
		srv *api.Server
		err error
	)

	log.Infof("Starting %s daemon. Core version: %s", pkg.NAME, pkg.VERSION)

	//Enable tracing and profiling
	if cfg.LogLevel() == 8 {
		log.CritOnErr(startProfiler, "can't start profiler:")
	}

	//Starting daemon
	w := worker.NewDaemon(p, pollInterval)
	if err = w.Run(cfg.Threads(), cfg.MaxQueue()); err != nil {
		log.Critf("Can't start internal health check daemon: %v", err)
	}

	// Starting API server
	if config.API().Enabled() {
		srv, err = api.NewServer()
		if err != nil {
			log.Critf("Can't start api server: %v", err)
		}
		srv.ListenAndServe()
	}

	// Shutdown by signals from OS
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig

	w.Stop()

	// Stop influx db if enabled
	if cfg.InfluxDBEnabled() {
		log.ErrOnErr(influxdb.Shutdown)
	}

	// Stop API if enabled
	if config.API().Enabled() {
		log.ErrOnErr(srv.Shutdown)
	}
}

func startProfiler() (err error) {
	// Init log folder
	path := config.Daemon().LogPath()
	if err = os.MkdirAll(path, 0755); err != nil {
		return err
	}

	var cpuFH *os.File
	cpuFH, err = os.Create(path + fmt.Sprintf("/%s.cpu.prof", pkg.NAME))
	if err != nil {
		return err
	}
	defer log.CritOnErr(cpuFH.Close, "Can't close CPU profile:")

	if err = pprof.StartCPUProfile(cpuFH); err != nil {
		return err
	}
	defer pprof.StopCPUProfile()

	var memFH *os.File
	memFH, err = os.Create(path + fmt.Sprintf("/%s.mem.prof", pkg.NAME))
	if err != nil {
		return err
	}
	runtime.GC() // get up-to-date statistics
	if err = pprof.WriteHeapProfile(memFH); err != nil {
		return err
	}
	defer log.CritOnErr(memFH.Close, "Can't close MEM profile:")

	go func() {
		grFH, errProfGR := os.Create(path + fmt.Sprintf("/%s.threads.stack", pkg.NAME))
		if errProfGR != nil {
			log.Critf("Can't create threads stack log: %v", errProfGR)
		}
		defer log.CritOnErr(grFH.Close, "Can't close stack profile:")

		for {
			if err = pprof.Lookup("goroutine").WriteTo(grFH, 1); err != nil {
				log.Errf("Can't write stack: %v", err)
			}

			time.Sleep(time.Second * 5)
		}
	}()

	return nil
}
