package pkg

import (
	"fmt"
	"strconv"
)

// Attribute - implementing abstract object for put/get any key/value params.
type Attribute map[string]interface{}

// GetString - returning string if key present, if type not are string will try convert it to string.
//  returning empty string if key not exist or type not cased.
func (a Attribute) GetString(key string) (s string) {
	if v, ok := a[key]; ok {
		switch val := v.(type) {
		case string:
			s = val
		case []byte:
			s = fmt.Sprintf("%s", val)
		case int:
			s = strconv.Itoa(val)
		case bool:
			s = strconv.FormatBool(val)
		}
	}

	return
}

// GetStrings - returning slice of strings if key present.
func (a Attribute) GetStrings(key string) (s []string) {
	if v, ok := a[key]; ok {
		switch val := v.(type) {
		case []string:
			s = val
		}
	}

	return
}

// GetInt - returning int if key present, if type not are int or key not present, will return 0.
func (a Attribute) GetInt(key string) (i int) {
	if v, ok := a[key]; ok {
		switch val := v.(type) {
		case int:
			i = val
		}
	}

	return
}

// GetUInt64 - returning int if key present, if type not are int or key not present, will return 0.
func (a Attribute) GetUInt64(key string) (i uint64) {
	if v, ok := a[key]; ok {
		switch val := v.(type) {
		case uint64:
			i = val
		}
	}

	return
}

// GetBool - returning bool if key present, if type not are bool or key not present, will return false.
func (a Attribute) GetBool(key string) (b bool) {
	if v, ok := a[key]; ok {
		switch val := v.(type) {
		case bool:
			b = val
		}
	}

	return
}
