package config

import (
	"fmt"
	"io/ioutil"

	"github.com/neliseev/go-config"
	"github.com/safervpn/saferwatcher/pkg/plugins/system"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/cpu"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/hdd"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/la"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/mem"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/uptime"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/ikev2"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/l2tp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/openvpn"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/pptp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/proxy"
)

var p *params

type params struct {
	FieldDaemon  *daemon        `yaml:"daemon"` // Including main params
	FieldAPI     *api           `yaml:"api"`    // Including api params
	FieldPlugins *pluginsParams `yaml:"plugins"`
}

type pluginsParams struct {
	System *system.Config `yaml:"system"`
	VPN    *vpn.Config    `yaml:"vpn"`
}

// Build func - building config from file path and env.
func Build(path string) error {
	p = &params{
		FieldDaemon: new(daemon),
		FieldAPI:    new(api),
		FieldPlugins: &pluginsParams{
			System: &system.Config{
				CPU:    new(cpu.Config),
				LA:     new(la.Config),
				Uptime: new(uptime.Config),
				Mem:    new(mem.Config),
				Hdd:    new(hdd.Config),
			},
			VPN: &vpn.Config{
				IKE2:  new(ikev2.Config),
				L2TP:  new(l2tp.Config),
				OVPN:  make(map[string]*openvpn.Config),
				PPTP:  new(pptp.Config),
				PROXY: new(proxy.Config),
			},
		},
	}

	switch data, err := ioutil.ReadFile(path); err != nil {
	case true:
		return fmt.Errorf("can't read config file: %v", err)
	default:
		return config.Parse(data, p)
	}
}

func Daemon() *daemon               { return p.FieldDaemon }
func API() *api                     { return p.FieldAPI }
func SystemPlugins() *system.Config { return p.FieldPlugins.System }
func VPNPlugins() *vpn.Config       { return p.FieldPlugins.VPN }
