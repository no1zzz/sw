package config

const (
	defThreads       = 2
	defInfluxDBHost  = "http://localhost:8086"
	defInfluxDBName  = "saferwatcher"
	defInfluxDBLogin = "saferwatcher"
	defMaxQueue      = 256
	defLogPath       = "/var/log/saferwatcher"
	defLogLevel      = 7
)

type daemon struct {
	FieldThreads          int    `yaml:"threads"`
	FieldMaxQueue         int    `yaml:"max_queue"`
	FieldInfluxDBEnabled  bool   `yaml:"influx_db_enabled"`
	FieldInfluxDBHost     string `yaml:"influx_db_host"`
	FieldInfluxDBName     string `yaml:"influx_db_name"`
	FieldInfluxDBLogin    string `yaml:"influx_db_login"`
	FieldInfluxDBPassword string `yaml:"influx_db_password"`
	FieldLogPath          string `yaml:"log_path"`
	FieldLogLevel         int    `yaml:"log_level"`
	FieldLogFile          string `yaml:"log_file"`
}

func (cfg *daemon) Threads() int {
	switch cfg.FieldThreads {
	case 0:
		return defThreads
	default:
		return cfg.FieldThreads
	}
}

func (cfg *daemon) MaxQueue() int {
	switch cfg.FieldMaxQueue {
	case 0:
		return defMaxQueue
	default:
		return cfg.FieldMaxQueue
	}
}

func (cfg *daemon) InfluxDBEnabled() bool { return cfg.FieldInfluxDBEnabled }

func (cfg *daemon) InfluxDBHost() string {
	switch cfg.FieldInfluxDBHost {
	case "":
		return defInfluxDBHost
	default:
		return cfg.FieldInfluxDBHost
	}
}

func (cfg *daemon) InfluxDBName() string {
	switch cfg.FieldInfluxDBName {
	case "":
		return defInfluxDBName
	default:
		return cfg.FieldInfluxDBName
	}
}

func (cfg *daemon) InfluxDBLogin() string {
	switch cfg.FieldInfluxDBLogin {
	case "":
		return defInfluxDBLogin
	default:
		return cfg.FieldInfluxDBLogin
	}
}

func (cfg *daemon) InfluxDBPass() string {
	return cfg.FieldInfluxDBPassword
}

func (cfg *daemon) LogPath() string {
	switch cfg.FieldLogPath {
	case "":
		return defLogPath
	default:
		return cfg.FieldLogPath
	}
}

func (cfg *daemon) LogLevel() int {
	switch cfg.FieldLogLevel {
	case 0:
		return defLogLevel
	default:
		return cfg.FieldLogLevel
	}
}

func (cfg *daemon) LogFile() string { return cfg.FieldLogFile }
