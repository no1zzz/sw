package config

var defACLAllowCIDR = []string{"127.0.0.1/32"}

const (
	defListen  = ":8443"
	defSSLCert = "./ssl.crt"
	defSSLKey  = "./ssl.key"
)

type api struct {
	FieldEnabled      bool     `yaml:"enabled"`
	FieldListen       string   `yaml:"listen"`
	FieldSSLCert      string   `yaml:"ssl_cert"`
	FieldSSLKey       string   `yaml:"ssl_key"`
	FieldACLAllowCIDR []string `yaml:"acl_allow_cidr"`
	FieldACLAllowAWS  bool     `yaml:"acl_allow_aws"`
}

func (cfg *api) Enabled() bool { return cfg.FieldEnabled }

func (cfg *api) Listen() string {
	switch cfg.FieldListen {
	case "":
		return defListen
	default:
		return cfg.FieldListen
	}
}

func (cfg *api) SSLCert() string {
	switch cfg.FieldSSLCert {
	case "":
		return defSSLCert
	default:
		return cfg.FieldSSLCert
	}

}

func (cfg *api) SSLKey() string {
	switch cfg.FieldSSLKey {
	case "":
		return defSSLKey
	default:
		return cfg.FieldSSLKey
	}
}

func (cfg *api) ACLAllowCIDR() []string {
	switch len(cfg.FieldACLAllowCIDR) {
	case 0:
		return defACLAllowCIDR
	default:
		return cfg.FieldACLAllowCIDR
	}
}

func (cfg *api) ACLAllowAWS() bool { return cfg.FieldACLAllowAWS }
