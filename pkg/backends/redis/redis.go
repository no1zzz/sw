package redis

import (
	"fmt"
	"sync"

	"github.com/go-redis/redis"
	"github.com/neliseev/logger"
)

var (
	self *Client
	log  *logger.Log
)

const (
	url           = "127.0.0.1:6379"
	ErrorNotFound = "redis: nil"
)

// Client - implementing connection to consul api.
type Client struct {
	*redis.Client
	sync.RWMutex
}

// Connect function - returning pointer to new Client type for each call.
// Expect:
//  addr - <fqdn|ip>:<port>, by default 127.0.0.1:6379.
func Connect(addr string) (err error) {
	log.Debugf("Redis client, connecting to: %s", addr)
	self = new(Client)
	self.Client = redis.NewClient(&redis.Options{Addr: addr})
	pong, err := self.Ping().Result()
	if err != nil {
		return fmt.Errorf("redis client, can't connect: %s", err)
	}
	log.Debug("Redis client, connected to: %s, pong: %s", addr, pong)

	return nil
}

// NewClient - returning pointer to current connection and error if client not connected.
func NewClient() (*Client, error) {
	if self == nil {
		if err := Connect(url); err != nil {
			return nil, err
		}
	}

	return self, nil
}

func Shutdown() error {
	self.Lock()
	defer self.Unlock()

	return self.Close()
}
