package influxdb

import (
	"fmt"
	"sync"
	"time"

	"github.com/influxdata/influxdb/client/v2"
	"github.com/neliseev/logger"
)

var (
	log  *logger.Log // Using log subsystem
	self *Client
)

// Client - implementing connection to consul api.
type Client struct {
	client.Client
	sync.RWMutex
}

// Connect function - returning pointer to new Client type for each call.
func Connect(addr, username, password string) (err error) {
	log.Debugf("InfluxDB client, connecting to: %s", addr)
	self = new(Client)

	cfg := client.HTTPConfig{
		Addr:     addr,
		Username: username,
		Password: password,
	}

	if self.Client, err = client.NewHTTPClient(cfg); err != nil {
		return fmt.Errorf("influxdb client, can't connect: %v", err)
	}
	log.Debug("InfluxDB client, connected to: %s", addr)

	_, _, err = self.Ping(time.Second)

	return err
}

// NewClient - returning pointer to current connection and error if client not connected.
func NewClient() (*Client, error) {
	if self == nil {
		return nil, fmt.Errorf("client not connected: %v", self)
	}

	return self, nil
}

func Shutdown() error {
	return self.Close()
}
