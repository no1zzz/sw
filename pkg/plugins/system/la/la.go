package la

import (
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/shirou/gopsutil/load"
)

var log *logger.Log // Using log subsystem

const NAME = "LA"
const ResultLa = "la"

type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	log.Debugf("%s: calculating loadaverage", NAME)
	newla := new(load.AvgStat)
	la := newla.Load1
	log.Debugf("%s: current LA: %+v", NAME, la)

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			ResultLa: la,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
