package uptime

import (
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/shirou/gopsutil/host"
)

var log *logger.Log // Using log subsystem

const NAME = "UPTIME"
const ResultUptime = "uptime"

type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var uptime int

	log.Debugf("%s: getting system uptime", NAME)
	uptimeuint64, err := host.Uptime()
	if err != nil {
		log.Debugf("%s: can't get data %v", NAME, err)
	}

	log.Debugf("%s: calculating uptime in hours", NAME)
	uptime = int(uptimeuint64) / 3600
	log.Debugf("%s: %v", NAME, uptime)

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			ResultUptime: uptime,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
