package mem

import (
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/shirou/gopsutil/mem"
)

var log *logger.Log // Using log subsystem

const NAME = "MEM"
const ResultMem = "mem"

type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	log.Debugf("%s: calculating mem usage", NAME)

	newmem, err := mem.VirtualMemory()
	if err != nil {
		return nil, err
	}
	mem := newmem.UsedPercent
	log.Debugf("%s: current Memory usage in percentage: %+v", NAME, mem)

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			ResultMem: mem,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
