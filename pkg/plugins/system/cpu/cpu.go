package cpu

import (
	"fmt"

	linuxproc "github.com/c9s/goprocinfo/linux"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
)

var log *logger.Log // Using log subsystem

const (
	NAME = "CPU"

	ParamProcStat = "procStat"

	attrTotalLoad = "totalLoad"
	attrWorkLoad  = "workLoad"

	ResultCPUUsage = "usage"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var cpuUsage uint64

	log.Debugf("%s: getting from in-mem db last result", NAME)
	lastResult, err := controller.GetMetric(NAME)
	if err != nil {
		return nil, err
	}
	log.Debugf("%s: last result from in-mem: %+v", NAME, lastResult)

	log.Debugf("%s: calculating last total and work load", NAME)
	lastTotalLoad := lastResult.Data.GetUInt64(attrTotalLoad)
	lastWorkLoad := lastResult.Data.GetUInt64(attrWorkLoad)
	log.Debugf("%s: last load, total: %v, work: %v", NAME, lastTotalLoad, lastWorkLoad)

	log.Debugf("%s: opening proc file system", NAME)
	ps, err := linuxproc.ReadStat(hc.params.GetString(ParamProcStat))
	if err != nil {
		return nil, fmt.Errorf("can't read proc system: %v", err)
	}
	log.Debugf("%s: proc file system opened success", NAME)

	log.Debugf("%s: calculating total load", NAME)
	totalLoad := ps.CPUStatAll.Nice + ps.CPUStatAll.User + ps.CPUStatAll.System + ps.CPUStatAll.Idle +
		ps.CPUStatAll.Guest + ps.CPUStatAll.IOWait + ps.CPUStatAll.SoftIRQ + ps.CPUStatAll.Steal
	log.Debugf("%s: total load: %v", NAME, totalLoad)

	log.Debugf("%s: calculating work load", NAME)
	workLoad := ps.CPUStatAll.System + ps.CPUStatAll.User + ps.CPUStatAll.Nice
	log.Debugf("%s: work load: %v", NAME, workLoad)

	if lastTotalLoad != 0 && lastWorkLoad != 0 {
		log.Debugf("%s: calculating usage", NAME)
		workLoadAVG := workLoad - lastWorkLoad
		totalLoadAVG := totalLoad - lastTotalLoad
		cpuUsage = uint64((float64(workLoadAVG) / float64(totalLoadAVG)) * 100)
		log.Debugf("%s: usage calculated, uint64((float64(%v) / float64(%v)) * 100) = %v",
			NAME, workLoadAVG, totalLoadAVG, cpuUsage)
	}

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			attrTotalLoad:  int64(totalLoad),
			attrWorkLoad:   int64(workLoad),
			ResultCPUUsage: int64(cpuUsage),
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
