package cpu

const (
	defProcStatFilePath = "/proc/stat"
	defWarnLevel        = 80
	defCritLevel        = 100
)

type Config struct {
	FieldEnabled          bool   `yaml:"enabled"`
	FieldProcStatFilePath string `yaml:"proc_stat"`
	FieldWarnLevel        int    `yaml:"warn_level"`
	FieldCritLevel        int    `yaml:"crit_level"`
}

func (cfg *Config) Enabled() bool { return cfg.FieldEnabled }

func (cfg *Config) ProcStatFilePath() string {
	switch cfg.FieldProcStatFilePath {
	case "":
		return defProcStatFilePath
	default:
		return cfg.FieldProcStatFilePath
	}
}

func (cfg *Config) WarnLevel() int {
	switch cfg.FieldWarnLevel {
	case 0:
		return defWarnLevel
	default:
		return cfg.FieldWarnLevel
	}
}

func (cfg *Config) CritLevel() int {
	switch cfg.FieldCritLevel {
	case 0:
		return defCritLevel
	default:
		return cfg.FieldCritLevel
	}
}
