package system

import (
	"github.com/safervpn/saferwatcher/pkg/plugins/system/cpu"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/hdd"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/la"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/mem"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/uptime"
)

type Config struct {
	CPU    *cpu.Config    `yaml:"cpu"`
	LA     *la.Config     `yaml:"la"`
	Uptime *uptime.Config `yaml:"uptime"`
	Mem    *mem.Config    `yaml:"mem"`
	Hdd    *hdd.Config    `yaml:"hdd"`
}
