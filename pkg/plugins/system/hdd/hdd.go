package hdd

import (
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/shirou/gopsutil/disk"
)

var log *logger.Log // Using log subsystem

const NAME = "HDD"
const ResultHdd = "hdd"

type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	log.Debugf("%s: calculating hdd usage", NAME)

	newhdd, err := disk.Usage("/")
	hdd := newhdd.UsedPercent
	if err != nil {
		log.Debugf("fck fck", err)
	}
	log.Debugf("%s: current hdd usage : %+v", NAME, hdd)

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			ResultHdd: hdd,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
