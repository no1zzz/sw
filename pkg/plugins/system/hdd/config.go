package hdd

const (
	defWarnLevel = 80
	defCritLevel = 100
)

type Config struct {
	FieldEnabled   bool `yaml:"enabled"`
	FieldWarnLevel int  `yaml:"warn_level"`
	FieldCritLevel int  `yaml:"crit_level"`
}

func (cfg *Config) Enabled() bool { return cfg.FieldEnabled }

func (cfg *Config) WarnLevel() int {
	switch cfg.FieldWarnLevel == 0 {
	case true:
		return defWarnLevel
	default:
		return cfg.FieldWarnLevel
	}
}

func (cfg *Config) CritLevel() int {
	switch cfg.FieldCritLevel == 0 {
	case true:
		return defCritLevel
	default:
		return cfg.FieldCritLevel
	}
}
