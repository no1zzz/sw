package plugins

import "github.com/safervpn/saferwatcher/pkg/controller"

// HealthChecker - implementing health check input contract.
//
// Run() - collecting metrics and return result.
type HealthChecker interface {
	Name() string
	Run() (metric *controller.Metric, err error)
}
