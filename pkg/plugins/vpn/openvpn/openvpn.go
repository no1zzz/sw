package openvpn

import (
	"bufio"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/supervisor"
)

var log *logger.Log // Using log subsystem

const (
	NAME           = "openvpn"
	RunningKey     = "running"
	usersKey       = "users"
	ovpnRxSpdKey   = "ovpnRxSpd"
	ovpnTxSpdKey   = "ovpnTxSpd"
	ovpnRxDataKey  = "ovpnRxData"
	ovpnTxDataKey  = "ovpnTxData"
	ParamProcName  = "procName"
	ParamStatsFile = "statusFile"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
//  metrics - data that we save in memdb from input plugin.
type HealthCheck struct {
	name   string
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var (
		users      uint
		ovpnRxSpd  int
		ovpnRxData int
		ovpnTxtmp  int
		ovpnTxSpd  int
		ovpnTxData int
		ovpnRxtmp  int
		running    int
	)

	ProcName := hc.params.GetString(ParamProcName)
	log.Debugf("%s: proc name %s:", NAME, ProcName)
	StatsFile := hc.params.GetString(ParamStatsFile)
	log.Debugf("%s: stat file %s:", NAME, StatsFile)

	log.Debugf("%s: getting from in-mem db last result", ProcName)
	lastResult, err := controller.GetMetric(ProcName)
	if err != nil {
		return nil, err
	}

	lastOvpnRxData := lastResult.Data.GetInt(ovpnRxDataKey)
	lastOvpnTxData := lastResult.Data.GetInt(ovpnTxDataKey)
	log.Debugf("%s: %s last OvpnRxData: %v, last OvpnTxData: %v", NAME, ProcName, lastOvpnRxData, lastOvpnTxData)

	log.Debugf("%s: getting data from statsfile", NAME)
	fh, err := os.Open(StatsFile)
	if err != nil {
		log.Debugf("%s can't open Stats file %s", NAME, err)
		return nil, err
	}
	defer log.ErrOnErr(fh.Close)

	reHeaderTop := regexp.MustCompile(`^?(OpenVPN|Updated|Common|UNDEF)`)
	reHeaderEnd := regexp.MustCompile(`^ROUTING`)

	log.Debugf("%s: calculating data from statsfile %s", NAME, StatsFile)
	for scan := bufio.NewScanner(fh); scan.Scan(); {
		line := scan.Text()
		if reHeaderTop.MatchString(line) {
			continue
		}
		if reHeaderEnd.MatchString(line) {
			break
		}
		data := strings.Split(line, ",")
		users++

		ovpnRxtmp, err = strconv.Atoi(data[2])
		if err != nil {
			log.Debugf("%s can't convert string to int %s", NAME, err)
			return nil, err
		}
		ovpnTxtmp, err = strconv.Atoi(data[3])
		if err != nil {
			log.Debugf("%s can't convert string to int %s", NAME, err)
			return nil, err
		}
		ovpnRxData += ovpnRxtmp
		ovpnTxData += ovpnTxtmp
	}
	log.Debugf("%s: %s: temporary ovpnRxData %v: temporary ovpnTxData %v", NAME, ProcName, ovpnRxData, ovpnTxData)

	log.Debugf("%s: calculating current network speed by ovpn protocol in bytes/seconds", NAME)
	ovpnRxSpd = (ovpnRxData - lastOvpnRxData) / 15
	ovpnTxSpd = (ovpnTxData - lastOvpnTxData) / 15
	log.Debugf("%s: %s: current ovpnRxSpd %v: current ovpnTxSpd %v", NAME, ProcName, ovpnRxSpd, ovpnTxSpd)

	log.Debugf("%s: %s: getting process status", NAME, ProcName)
	running, err = supervisor.CheckSupervisor(ProcName)
	if err != nil {
		log.Debugf("%s: %s: cant get status %v", NAME, ProcName, running)
	}

	metric := &controller.Metric{
		Name: ProcName,
		Data: pkg.Attribute{
			usersKey:      int(users),
			ovpnRxSpdKey:  ovpnRxSpd,
			ovpnTxSpdKey:  ovpnTxSpd,
			ovpnRxDataKey: ovpnRxData,
			ovpnTxDataKey: ovpnTxData,
			RunningKey:    running,
		},
	}
	log.Debugf("%s: %s: writing metric into in-mem db, data: %+v", NAME, ProcName, metric)

	return metric, nil

}
