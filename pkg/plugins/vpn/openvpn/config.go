package openvpn

import "fmt"

type Config struct {
	FieldEnabled            bool   `yaml:"enabled"`
	FieldStatsFilePath      string `yaml:"stat_file"`
	FieldSupervisorProcName string `yaml:"supervisor_proc_name"`
}

func (cfg *Config) Enabled() bool { return cfg.FieldEnabled }

func (cfg *Config) StatFile() (string, error) {
	if cfg.FieldStatsFilePath == "" {
		return "", fmt.Errorf("status file can't be null: %s", cfg.FieldStatsFilePath)
	}

	return cfg.FieldStatsFilePath, nil
}

func (cfg *Config) ProcName() (string, error) {
	if cfg.FieldSupervisorProcName == "" {
		return "", fmt.Errorf("Supervisor Proc Name can't be null: %s", cfg.FieldSupervisorProcName)
	}

	return cfg.FieldSupervisorProcName, nil
}
