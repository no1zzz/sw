package proxy

import (
	"strconv"

	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/backends/redis"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/supervisor"
)

var log *logger.Log // Using log subsystem

const (
	NAME           = "PROXY"
	proxyRxSpdKey  = "proxyRxSpd"
	proxyTxSpdKey  = "proxyTxSpd"
	proxyRxDataKey = "proxyRxData"
	proxyTxDataKey = "proxyTxData"
	RunningKey     = "running"
	usersKey       = "users"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
//  metrics - data that we save in memdb from input plugin.
type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {

	var (
		cursor      uint64
		proxyTxData int
		proxyRxData int
		proxyTxSpd  int
		proxyRxSpd  int
		users       int
		running     int
	)

	client, err := redis.NewClient()
	if err != nil {
		log.Debugf("%s can't connect to redis ", NAME, err)
	}

	log.Debugf("%s: getting from in-mem db last result", NAME)
	lastResult, err := controller.GetMetric(NAME)
	if err != nil {
		return nil, err
	}

	lastProxyRxData := lastResult.Data.GetInt(proxyRxDataKey)
	lastProxyTxData := lastResult.Data.GetInt(proxyTxDataKey)
	log.Debugf("%s: last pptpRxData: %v, last pptp2TxData: %v", NAME, lastProxyRxData, lastProxyTxData)

	iterTx := client.Scan(cursor, ":stat_tx_*", 100).Iterator()
	for iterTx.Next() {
		rawVal, err := client.Get(iterTx.Val()).Bytes()
		if err != nil {
			return nil, err
		}
		v, err := strconv.Atoi(string(rawVal))
		log.Debugf("%s: value of key in redis", NAME, v)
		if err != nil {
			log.Debugf("%s: get error in value %s", NAME, v)
		}
		proxyTxData += v
	}
	if err := iterTx.Err(); err != nil {
		log.Crit(err)
	}
	log.Debug("%s: proxyTxData: %d", NAME, proxyTxData)

	iterRx := client.Scan(cursor, ":stat_rx_*", 100).Iterator()
	for iterRx.Next() {
		rawVal, err := client.Get(iterRx.Val()).Bytes()
		if err != nil {
			return nil, err
		}
		v, err := strconv.Atoi(string(rawVal))
		log.Debugf("%s: value of key in redis", NAME, v)
		if err != nil {
			log.Debugf("%s: get error in value %s", NAME, v)
		}
		proxyRxData += v
	}
	if err := iterRx.Err(); err != nil {
		log.Crit(err)
	}
	log.Debug("%s: proxyRxData: %d", NAME, proxyRxData)

	iterUser := client.Scan(cursor, ":stat_on_*", 100).Iterator()
	for iterUser.Next() {
		rawVal, err := client.Get(iterUser.Val()).Bytes()
		if err != nil {
			return nil, err
		}
		v, err := strconv.Atoi(string(rawVal))
		log.Debugf("%s: value of key in redis", NAME, v)
		if err != nil {
			log.Debugf("%s get error in value %s", NAME, v)
		}
		users += v
		log.Debugf("%s stat_on: %v", NAME, users)
	}
	if err := iterUser.Err(); err != nil {
		log.Crit(err)
	}
	log.Debugf("%s Users: %v", NAME, users)

	log.Debugf("%s: calculating current network speed by proxy protocol in bytes/seconds", NAME)
	proxyRxSpd = (proxyRxData - lastProxyRxData) / 15
	proxyTxSpd = (proxyTxData - lastProxyTxData) / 15

	log.Debugf("%s: getting process status", NAME)
	running, err = supervisor.CheckSupervisor("squid")
	if err != nil {
		log.Debugf("%s: cant get status of proxy %s", NAME, running)
	}

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			usersKey:       users,
			RunningKey:     running,
			proxyRxDataKey: proxyRxData,
			proxyTxDataKey: proxyTxData,
			proxyRxSpdKey:  proxyRxSpd,
			proxyTxSpdKey:  proxyTxSpd,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, err
}
