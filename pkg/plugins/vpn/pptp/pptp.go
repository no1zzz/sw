package pptp

import (
	"bufio"
	"bytes"
	"os/exec"
	"regexp"
	"strings"

	netstat "github.com/jmslocum/netstats"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/supervisor"
)

var log *logger.Log // Using log subsystem

const (
	NAME          = "PPTP"
	pptpRxSpdKey  = "pptpRxSpd"
	pptpTxSpdKey  = "pptpTxSpd"
	pptpRxDataKey = "pptpRxData"
	pptpTxDataKey = "pptpTxData"
	RunningKey    = "running"
	usersKey      = "users"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
//  metrics - data that we save in memdb from input plugin.
type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var (
		users      uint
		pptpRxSpd  uint64
		pptpRxData uint64
		pptpTxSpd  uint64
		pptpTxData uint64
		iface      string
		running    int
	)

	log.Debugf("%s: getting from in-mem db last result", NAME)
	lastResult, err := controller.GetMetric(NAME)
	if err != nil {
		return nil, err
	}

	lastPptpRxData := lastResult.Data.GetUInt64(pptpRxDataKey)
	lastPptpTxData := lastResult.Data.GetUInt64(pptpTxDataKey)
	log.Debugf("%s: last pptpRxData: %v, last pptp2TxData: %v", NAME, lastPptpRxData, lastPptpTxData)

	log.Debugf("%s: getting data from pptp", NAME)
	cmd := exec.Command("last")
	var out []byte
	var stderr bytes.Buffer
	out, err = cmd.Output()
	if err != nil {
		log.Debugf("%s: err in data output: ", NAME, stderr.String())
	}
	cmd.Stderr = &stderr
	pptplast := string(out)

	reLast := regexp.MustCompile(`((ppp\d+).*(gone\s-\sno\slogout))`)

	log.Debugf("%s: calculating users", NAME)
	for scan := bufio.NewScanner(strings.NewReader(pptplast)); scan.Scan(); {
		line := scan.Text()
		if reLast.MatchString(line) {
			iface = reLast.FindAllStringSubmatch(line, 2)[0][2]
			log.Debugf("%s: ppp iface %s", NAME, iface)
			users++
			log.Debugf("%s: users count on exit from regexp %v", NAME, line)

			stats, errNet := netstat.StatsByName(iface)
			if errNet != nil {
				break
			}
			pptprxtmp := uint64(stats.RxStats[0])
			pptptxtmp := uint64(stats.TxStats[0])
			pptpRxData += pptprxtmp
			pptpTxData += pptptxtmp
		}
	}

	log.Debugf("%s: calculating current network speed by pptp protocol in bytes/seconds", NAME)
	pptpRxSpd = (pptpRxData - lastPptpRxData) / 15
	pptpTxSpd = (pptpTxData - lastPptpTxData) / 15

	log.Debugf("%s: getting process status", NAME)
	running, err = supervisor.CheckSupervisor("pptpd")
	if err != nil {
		log.Debugf("%s: cant get status of pptp %s", NAME, running)
	}

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			usersKey:      int(users),
			pptpRxSpdKey:  int64(pptpRxSpd),
			pptpTxSpdKey:  int64(pptpTxSpd),
			pptpRxDataKey: int64(pptpRxData),
			pptpTxDataKey: int64(pptpTxData),
			RunningKey:    running,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
