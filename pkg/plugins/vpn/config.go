package vpn

import (
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/ikev2"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/l2tp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/openvpn"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/pptp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/proxy"
)

type Config struct {
	IKE2  *ikev2.Config              `yaml:"ike2"`
	L2TP  *l2tp.Config               `yaml:"l2tp"`
	OVPN  map[string]*openvpn.Config `yaml:"ovpn"` // key should be like port protocol (1194upd)
	PPTP  *pptp.Config               `yaml:"pptp"`
	PROXY *proxy.Config              `yaml:"proxy"`
}
