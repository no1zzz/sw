package ikev2

type Config struct {
	FieldEnabled bool `yaml:"enabled"`
}

func (cfg *Config) Enabled() bool { return cfg.FieldEnabled }
