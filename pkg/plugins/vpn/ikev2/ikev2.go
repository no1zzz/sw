package ikev2

import (
	"bufio"
	"bytes"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/bronze1man/goStrongswanVici"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/supervisor"
)

var log *logger.Log // Using log subsystem

const (
	NAME           = "IKEV2"
	ikev2RxSpdKey  = "ikev2RxSpd"
	ikev2TxSpdKey  = "ikev2TxSpd"
	ikev2RxDataKey = "ikev2RxData"
	ikev2TxDataKey = "ikev2TxData"
	RunningKey     = "running"
	usersKey       = "users"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var (
		users       uint
		ikev2RxSpd  uint32
		ikev2RxData int
		ikev2Rxtmp  int
		ikev2TxSpd  uint32
		ikev2TxData int
		ikev2Txtmp  int
		running     int
	)

	log.Debugf("%s: getting from in-mem db last result", NAME)
	lastResult, err := controller.GetMetric(NAME)
	if err != nil {
		return nil, err
	}

	log.Debugf("%s: getting process status", NAME)
	running, err = supervisor.CheckSupervisor("ikev2")
	if err != nil {
		log.Debugf("%s: cant get status %v", NAME, running)
		running = 0
	}

	if running != 0 {
		lastIkev2RxData := lastResult.Data.GetInt(ikev2RxDataKey)
		lastIkev2TxData := lastResult.Data.GetInt(ikev2TxDataKey)
		log.Debugf("%s: last ikev2RxData: %v, last ikev2TxData: %v", NAME, lastIkev2RxData, lastIkev2TxData)

		log.Debugf("%s: calculating data from ikev2 socket vici", NAME)
		// create a client.
		client, err := goStrongswanVici.NewClientConnFromDefaultSocket()
		if err != nil {
			log.Debugf("%s can't open stats socket %s", NAME, err)
			return nil, err
		}
		defer log.ErrOnErr(client.Close)

		var data []map[string]goStrongswanVici.IkeSa
		log.Debugf("%s: getting client listSas from vici socket", NAME)
		data, err = client.ListSas("", "")
		if err != nil {
			log.Debugf("%s: cant get client list %s", NAME, data)
		}

		log.Debugf("%s: calculating network speed", NAME)
		for _, v := range data {
			if val, ok := v["ikev2"]; ok {
				for _, rdata := range val.Child_sas {
					ikev2Rxtmp, err = strconv.Atoi(rdata.Bytes_in)
					if err != nil {
						log.Debugf("%s: no temp data %v", NAME, ikev2Rxtmp)
					}
					ikev2Txtmp, err = strconv.Atoi(rdata.Bytes_out)
					if err != nil {
						log.Debugf("%s: no temp data %v", NAME, ikev2Txtmp)
					}
					ikev2RxData += ikev2Rxtmp
					ikev2TxData += ikev2Txtmp
				}
			}
		}
		log.Debugf("%s: temporary ikev2RxData %v: temporary ikev2TxData %v", NAME, ikev2RxData, ikev2TxData)

		log.Debugf("%s: calculating current network speed by ikev2 protocol in bytes/seconds", NAME)
		ikev2RxSpd = uint32(ikev2RxData-lastIkev2RxData) / 15
		ikev2TxSpd = uint32(ikev2TxData-lastIkev2TxData) / 15
		log.Debugf("%s: current ikev2Rx %v: current ikev2Tx %v", NAME, ikev2RxSpd, ikev2TxSpd)

		log.Debugf("%s: getting status", NAME)
		args := []string{"status", "ikev2"}
		app := "ipsec"
		cmd := exec.Command(app, args...)
		var out []byte
		var stderr bytes.Buffer
		out, err = cmd.Output()
		if err != nil {
			log.Debugf("%s: err in data string: ", NAME, stderr.String())
		}
		cmd.Stderr = &stderr
		outikev2 := string(out)

		reHeaderTop := regexp.MustCompile(`(ESTABLISHED)`)
		reHeaderEnd := regexp.MustCompile(`(detected)`)

		log.Debugf("%s: calculating users", NAME)
		for scan := bufio.NewScanner(strings.NewReader(outikev2)); scan.Scan(); {
			line := scan.Text()
			if reHeaderTop.MatchString(line) {
				users++
				continue
			}
			if reHeaderEnd.MatchString(line) {
				break
			}
		}
	}

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			usersKey:       int(users),
			ikev2RxSpdKey:  int(ikev2RxSpd),
			ikev2TxSpdKey:  int(ikev2TxSpd),
			ikev2RxDataKey: ikev2RxData,
			ikev2TxDataKey: ikev2TxData,
			RunningKey:     running,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
