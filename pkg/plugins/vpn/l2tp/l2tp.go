package l2tp

import (
	"bufio"
	"bytes"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/bronze1man/goStrongswanVici"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins"
	"github.com/safervpn/saferwatcher/pkg/plugins/system/supervisor"
)

var log *logger.Log // Using log subsystem

const (
	NAME          = "L2TP"
	l2tpRxSpdKey  = "l2tpRxSpd"
	l2tpTxSpdKey  = "l2tpTxSpd"
	l2tpRxDataKey = "l2tpRxData"
	l2tpTxDataKey = "l2tpTxData"
	RunningKey    = "running"
	usersKey      = "users"
)

// HealthCheck - implementing health check.
//
// Fields:
//  params - pass or get params to input plugin.
//  metrics - data that we save in memdb from input plugin.
type HealthCheck struct {
	params pkg.Attribute
}

func NewHealthCheck(params pkg.Attribute) plugins.HealthChecker { return &HealthCheck{params: params} }

func (hc *HealthCheck) Name() string { return NAME }

// Run method - implementing input HealthChecker interface.
func (hc *HealthCheck) Run() (m *controller.Metric, err error) {
	var (
		users      uint
		l2tpRxSpd  uint32
		l2tpRxData int
		l2tpRxtmp  int
		l2tpTxSpd  uint32
		l2tpTxData int
		l2tpTxtmp  int
		running    int
	)

	log.Debugf("%s: getting from in-mem db last result", NAME)
	lastResult, err := controller.GetMetric(NAME)
	if err != nil {
		return nil, err
	}

	log.Debugf("%s: getting process status", NAME)
	running, err = supervisor.CheckSupervisor("xl2tpd")
	if err != nil {
		log.Debugf("%s: cant get status %v", NAME, running)
		running = 0
	}

	if running != 0 {
		lastL2tpRxData := lastResult.Data.GetInt(l2tpRxDataKey)
		lastL2tpTxData := lastResult.Data.GetInt(l2tpTxDataKey)
		log.Debugf("%s: last l2tpRxData: %v, last l2tpTxData: %v", NAME, lastL2tpRxData, lastL2tpTxData)

		log.Debugf("%s: calculating data from l2tp socket vici", NAME)
		// create a client.
		client, err := goStrongswanVici.NewClientConnFromDefaultSocket()
		if err != nil {
			log.Debugf("%s can't open stats socket %s", NAME, err)
			return nil, err
		}
		defer log.ErrOnErr(client.Close)

		log.Debugf("%s: getting client listSas from vici socket", NAME)
		data, err := client.ListSas("", "")
		if err != nil {
			log.Debugf("%s: cant get client list %s", NAME, data)
		}

		log.Debugf("%s: calculating network speed", NAME)
		for _, v := range data {
			if val, ok := v["l2tp"]; ok {
				for _, rdata := range val.Child_sas {
					l2tpRxtmp, err = strconv.Atoi(rdata.Bytes_in)
					if err != nil {
						log.Debugf("%s: no temp data %v", NAME, l2tpRxtmp)
					}

					l2tpTxtmp, err = strconv.Atoi(rdata.Bytes_out)
					if err != nil {
						log.Debugf("%s: no temp data %v", NAME, l2tpTxtmp)
					}
					l2tpRxData += l2tpRxtmp
					l2tpTxData += l2tpTxtmp
				}
			}
		}
		log.Debugf("%s: temporary l2tpRxData %v: temporary l2tpTxData %v", NAME, l2tpRxData, l2tpTxData)

		log.Debugf("%s: calculating current network speed by l2tp protocol in bytes/seconds", NAME)
		l2tpRxSpd = uint32(l2tpRxData-lastL2tpRxData) / 15
		l2tpTxSpd = uint32(l2tpTxData-lastL2tpTxData) / 15
		log.Debugf("%s: current l2tp2RxSpd %v: current l2tp2TxSpd %v", NAME, l2tpRxSpd, l2tpTxSpd)

		log.Debugf("%s: getting status", NAME)
		args := []string{"status", "l2tp"}
		app := "ipsec"
		cmd := exec.Command(app, args...)
		var out []byte
		var stderr bytes.Buffer
		out, err = cmd.Output()
		cmd.Stderr = &stderr
		if err != nil {
			log.Debugf("%s: err in data string: ", NAME, stderr.String())
		}
		outikev2 := string(out)

		reHeaderTop := regexp.MustCompile(`(INSTALLED)`)
		reHeaderEnd := regexp.MustCompile(`(detected)`)

		log.Debugf("%s: calculating users", NAME)
		for scan := bufio.NewScanner(strings.NewReader(outikev2)); scan.Scan(); {
			line := scan.Text()
			if reHeaderTop.MatchString(line) {
				users++
				continue
			}
			if reHeaderEnd.MatchString(line) {
				break
			}
		}
	}

	metric := &controller.Metric{
		Name: NAME,
		Data: pkg.Attribute{
			usersKey:      int(users),
			l2tpRxSpdKey:  int(l2tpRxSpd),
			l2tpTxSpdKey:  int(l2tpTxSpd),
			l2tpRxDataKey: l2tpRxData,
			l2tpTxDataKey: l2tpTxData,
			RunningKey:    running,
		},
	}
	log.Debugf("%s: writing metric into in-mem db, data: %+v", NAME, metric)

	return metric, nil
}
