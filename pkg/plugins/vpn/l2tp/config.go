package l2tp

type Config struct {
	FieldEnabled bool `yaml:"enabled"`
}

func (cfg *Config) Enabled() bool { return cfg.FieldEnabled }
