package worker

import (
	"sync"
	"time"

	"github.com/safervpn/saferwatcher/pkg/plugins"
)

type daemon struct {
	plugins      map[string]plugins.HealthChecker
	pollInterval time.Duration
	quit         chan bool
	sync.RWMutex
}

func NewDaemon(plugins map[string]plugins.HealthChecker, pollInterval time.Duration) *daemon {
	return &daemon{
		plugins:      plugins,
		pollInterval: pollInterval,
		quit:         make(chan bool),
	}
}

func (d *daemon) Run(maxWorkers, maxJobQueue int) error {
	d.Lock()
	defer d.Unlock()

	go func() {
		// Init main queue for workers.
		jobQueue = make(chan job, maxJobQueue)

		// Init job dispatcher.
		disp := newDispatcher(maxWorkers)
		disp.run()

		for {
			select {
			case <-d.quit:
				// we have received a signal to stop
				log.Debug("Shutdown internal worker")

				return
			case <-time.After(d.pollInterval):
				log.Debug("Worker starting new job's cycle, for plugins")
				for k, v := range d.plugins {
					log.Debugf("Dispatching plugin: %s", k)
					jobQueue <- job{plugin: v}
					log.Debugf("Plugin dispatched: %s", k)
				}
				log.Debug("Worker job's cycle done, for plugins")
			}
		}
	}()

	return nil
}

func (d *daemon) Stop() {
	d.Lock()
	defer d.Unlock()

	d.quit <- true
}
