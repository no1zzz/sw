package worker

import (
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg/plugins"
)

var log *logger.Log // Using log subsystem

// Job represents the job to be run
type job struct {
	plugin plugins.HealthChecker
}

// Worker represents the worker that executes the job
type worker struct {
	id         int
	workerPool chan chan job
	jobChannel chan job
	quit       chan bool
}

func newWorker(id int, workerPool chan chan job) worker {
	return worker{
		id:         id,
		workerPool: workerPool,
		jobChannel: make(chan job),
		quit:       make(chan bool),
	}
}

// Run method starts the run loop for the worker, listening for a quit channel in
// case we need to stop it
func (w worker) run() {
	go func() {
		log.Debugf("Worker started, id: %v", w.id)
		defer log.Debugf("Worker stopped, id: %v", w.id)

		for {
			// register the current worker into the worker queue.
			w.workerPool <- w.jobChannel

			select {
			case j := <-w.jobChannel:
				m, err := j.plugin.Run()
				if err != nil {
					log.Errf("Can't run job: %v", err)

					continue
				}

				if err = m.Put(); err != nil {
					log.Errf("Can't save metric: %v", err)

					continue
				}
			case <-w.quit:
				// we have received a signal to stop
				return
			}
		}
	}()
}

// Stop signals the worker to stop listening for work requests.
func (w worker) stop() {
	go func() {
		w.quit <- true
	}()
}
