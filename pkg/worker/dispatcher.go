package worker

var jobQueue chan job // internal buffered job's queue.

// dispatcher struct - implementing dispatcher for workers.
type dispatcher struct {
	maxWorkers  int
	workers     map[int]worker
	workersPool chan chan job
}

// newDispatcher func - creating new empty dispatcher.
func newDispatcher(maxWorkers int) *dispatcher {
	return &dispatcher{
		maxWorkers:  maxWorkers,
		workers:     make(map[int]worker),
		workersPool: make(chan chan job, maxWorkers),
	}
}

// run method - creating workers and registering their in pool.
func (d *dispatcher) run() {
	log.Debugf("Starting %v workers", d.maxWorkers)
	for i := 0; i < d.maxWorkers; i++ {
		log.Debugf("Starting worker, id: %v", i)
		d.workers[i] = newWorker(i, d.workersPool)
		d.workers[i].run()
		log.Debugf("Worker, id: %v, started", i)
	}

	go d.dispatch()
}

func (d *dispatcher) dispatch() {
	log.Debugf("Workers dispatcher started")
	for j := range jobQueue {
		log.Debug("dispatcher request received")
		// a job request has been received
		go func(j job) {
			// try to obtain a worker job channel that is available.
			// this will block until a worker is idle
			jobChannel := <-d.workersPool

			// dispatch the job to the worker job channel
			jobChannel <- j
		}(j)
	}
}
