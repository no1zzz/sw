package health

import (
	"encoding/json"
	"net/http"

	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg/config"
	"github.com/safervpn/saferwatcher/pkg/controller"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/ikev2"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/l2tp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/openvpn"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/pptp"
	"github.com/safervpn/saferwatcher/pkg/plugins/vpn/proxy"
)

var log *logger.Log

const (
	ok       = "OK"
	critical = "CRITICAL"
	unknown  = "UNKNOWN"
)

type result struct {
	Status string            `json:"status"`
	IKE2   string            `json:"ike2,omitempty"`
	L2TP   string            `json:"l2tp,omitempty"`
	OVPN   map[string]string `json:"ovpn,omitempty"`
	PPTP   string            `json:"pptp,omitempty"`
	PROXY  string            `json:"proxy,omitempty"`
}

func VPN(w http.ResponseWriter, r *http.Request) {
	var (
		cfg      = config.VPNPlugins()
		response = &result{
			Status: unknown,
			OVPN:   make(map[string]string),
		}

		errs    int
		enabled bool
	)

	if cfg.IKE2.Enabled() {
		enabled = true
		state := check(ikev2.NAME, ikev2.RunningKey)
		response.IKE2 = state

		if state != ok {
			errs++
		}
	}

	if cfg.L2TP.Enabled() {
		enabled = true
		state := check(l2tp.NAME, l2tp.RunningKey)
		response.L2TP = state

		if state != ok {
			errs++
		}
	}

	for k, v := range cfg.OVPN {
		if v.Enabled() {
			enabled = true
			procname, err := v.ProcName()
			if err != nil {
				log.Errf("%s: no proc name provided - key %s:", procname, k)

				continue
			}

			state := check(procname, openvpn.RunningKey)
			response.OVPN[procname] = state
			if state != ok {
				errs++
			}
		}
	}

	if cfg.PPTP.Enabled() {
		enabled = true
		state := check(pptp.NAME, pptp.RunningKey)
		response.PPTP = state

		if state != ok {
			errs++
		}
	}

	if cfg.PROXY.Enabled() {
		enabled = true
		state := check(proxy.NAME, proxy.RunningKey)
		response.PROXY = state

		if state != ok {
			errs++
		}
	}

	switch true {
	case errs == 0 && enabled:
		response.Status = ok
	case errs != 0 && enabled:
		response.Status = critical
	default:
		response.Status = unknown
	}

	handleResponse(w, r, response)
}

func check(name, runningKey string) (status string) {
	var (
		m   *controller.Metric
		err error
	)

	log.Debugf("Checking %s by key %s", name, runningKey)
	m, err = controller.GetMetric(name)
	if err != nil {
		log.Errf("Can't read metric %s from in-mem DB: %v", name, err)

		return unknown
	}

	if state := m.Data.GetInt(runningKey); state != 1 {
		log.Errf("%s not running, state: %v", name, state)

		return critical
	}

	return ok
}

func handleResponse(w http.ResponseWriter, r *http.Request, response *result) {
	w.Header().Set("Content-Type", "application/json")

	data, err := json.Marshal(response)
	if err != nil {
		log.Errf("Can't marshal result, call: %s, from: %s, err: %v", r.RequestURI, r.RemoteAddr, err)
		w.WriteHeader(http.StatusInternalServerError)

		if _, err = w.Write(nil); err != nil {
			log.Errf("Can't response for call: %s, from: %s, err: %v", r.RequestURI, r.RemoteAddr, err)
		}

		return
	}

	switch response.Status {
	case ok:
		w.WriteHeader(http.StatusOK)
	case critical:
		w.WriteHeader(http.StatusServiceUnavailable)
	case unknown:
		w.WriteHeader(http.StatusInternalServerError)
	default:
		w.WriteHeader(http.StatusBadRequest)
	}

	if _, err = w.Write(data); err != nil {
		log.Errf("Can't response for call: %s, from: %s, err: %v", r.RequestURI, r.RemoteAddr, err)
	}
}
