package mwf

import (
	"net"
	"net/http"

	"github.com/safervpn/saferwatcher/pkg/api/acl"
)

func ACL(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ip, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				log.Err(err)

				// Write an error and stop the handler chain
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}

			if acl.EvaluateIP(ip) {
				log.Debugf("Access granted from: %s", r.RemoteAddr)

				// Call the next handler, which can be another middleware in the chain, or the final handler.
				next.ServeHTTP(w, r)
			} else {
				log.Debugf("Access forbidden from: %s", r.RemoteAddr)

				// Write an error and stop the handler chain
				http.Error(w, "Forbidden", http.StatusForbidden)
			}
		},
	)
}
