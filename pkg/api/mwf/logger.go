package mwf

import (
	"net/http"

	"github.com/neliseev/logger"
)

var log *logger.Log

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			log.Debugf("uri: %s, from: %s", r.RequestURI, r.RemoteAddr)

			// Call the next handler, which can be another middleware in the chain, or the final handler.
			next.ServeHTTP(w, r)
		},
	)
}
