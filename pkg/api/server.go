package api

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg/api/acl"
	"github.com/safervpn/saferwatcher/pkg/api/handlers/v1/health"
	"github.com/safervpn/saferwatcher/pkg/api/mwf"
	"github.com/safervpn/saferwatcher/pkg/config"
)

var log *logger.Log

const (
	timeout   = time.Second * 30
	errClosed = "http: Server closed"
)

type Server struct {
	http *http.Server
	sync.RWMutex
}

func NewServer() (*Server, error) {
	var (
		cfg = config.API()
		err error
	)

	// Creating new multiplexer router
	r := mux.NewRouter()

	// Registering handlers
	r.HandleFunc("/api/v1/health/vpn", health.VPN)

	// Registering middleware functions
	r.Use(mwf.Logging)
	r.Use(mwf.ACL)

	// Initialization API ACL if API enabled
	cidr := make(map[string]bool)
	for _, v := range cfg.ACLAllowCIDR() {
		cidr[v] = true
	}

	if err = acl.Init(cidr, cfg.ACLAllowAWS()); err != nil {
		return nil, fmt.Errorf("can't init API ACL: %v", err)
	}

	return &Server{
		http: &http.Server{
			Addr:         cfg.Listen(),
			Handler:      r,
			ReadTimeout:  time.Second * 15,
			WriteTimeout: time.Second * 15,
			IdleTimeout:  time.Second * 5,
		},
	}, nil
}

func (srv *Server) ListenAndServe() {
	srv.Lock()
	defer srv.Unlock()

	go func() {
		cfg := config.API()
		err := srv.http.ListenAndServeTLS(cfg.SSLCert(), cfg.SSLKey())
		if err != nil && err.Error() != errClosed {
			log.Critf("Can't listen and serve: %v", err)
		}
	}()
}

func (srv *Server) Shutdown() error {
	srv.RLock()
	defer srv.RUnlock()

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	return srv.http.Shutdown(ctx)
}
