package acl

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"

	"github.com/neliseev/logger"
)

var (
	log *logger.Log
	acl = make(map[string]bool)
)

const (
	awsIPServicesUrl         = "https://ip-ranges.amazonaws.com/ip-ranges.json"
	awsR53HealthCheckService = "ROUTE53_HEALTHCHECKS"
)

type awsIPServices struct {
	SyncToken  string `json:"syncToken"`
	CreateDate string `json:"createDate"`
	Prefixes   []struct {
		IPPrefix string `json:"ip_prefix"`
		Region   string `json:"region"`
		Service  string `json:"service"`
	} `json:"prefixes"`
}

func Init(cidr map[string]bool, includeAWS bool) error {
	// Add into ACL AWS HealthCheck ip's
	if includeAWS {
		resp, err := http.Get(awsIPServicesUrl)
		if err != nil {
			return fmt.Errorf("can't get AWS IP's list form url: %s %v", awsIPServicesUrl, err)
		}
		defer log.ErrOnErr(resp.Body.Close)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("can't read http body: %v", err)
		}

		awsIPs := new(awsIPServices)
		if err = json.Unmarshal(body, awsIPs); err != nil {
			return fmt.Errorf("can't unmarshal json: %v", err)
		}

		for _, v := range awsIPs.Prefixes {
			if strings.ToUpper(v.Service) != awsR53HealthCheckService {
				continue
			}

			addSubnet(v.IPPrefix, true)
		}
	}

	// Add local ip's into acl.
	for k, v := range cidr {
		addSubnet(k, v)
	}

	return nil
}

func addSubnet(cidr string, allow bool) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		log.Errf("Can't parse subnet cidr: %s, %v", cidr, err)
	}

	for ip = ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		acl[ip.String()] = allow
	}
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func EvaluateIP(ip string) (allow bool) {
	var ok bool

	if allow, ok = acl[ip]; !ok {
		return false
	}

	return allow
}
