package controller

import (
	"time"

	"github.com/safervpn/saferwatcher/pkg"
	"github.com/safervpn/saferwatcher/pkg/model"
)

// MetricData type - providing simple metrics type for any input's plugins.
type Metric struct {
	Name string
	Data pkg.Attribute
}

// PutMetric func - inserting into model metric by name.
func (m *Metric) Put() error {
	return model.PutMetric(&model.Metric{Name: m.Name, Timestamp: time.Now(), State: m.Data})
}

// GetMetric func - returning controller *Metric by name.
func GetMetric(name string) (*Metric, error) {
	data, err := model.GetMetric(name)
	if err != nil {
		return nil, err
	}

	return &Metric{Name: name, Data: data.State}, nil
}
