package model

import (
	"fmt"
	"time"

	"github.com/hashicorp/go-memdb"
	"github.com/neliseev/logger"
	"github.com/safervpn/saferwatcher/pkg"
)

var (
	log *logger.Log
	db  *memdb.MemDB
)

const (
	idx      = "id"
	idxField = "Name"
	tblState = "state"
)

func InitInMemDB() error {
	var err error

	// Create the DB
	log.Debug("Initializing in-mem db")
	db, err = memdb.NewMemDB(
		&memdb.DBSchema{
			Tables: map[string]*memdb.TableSchema{
				tblState: {
					Name: tblState,
					Indexes: map[string]*memdb.IndexSchema{
						idx: {
							Name:    idx,
							Unique:  true,
							Indexer: &memdb.StringFieldIndex{Field: idxField},
						},
					},
				},
			},
		},
	)
	if err != nil {
		return err
	}
	log.Debug("In-mem db initialized")

	return nil
}

// Metric - in-mem db object for simple metric's
//
// Fields:
//  Name  - metric name, unique.
//  State - abstract object with attributes, aka metric data.
type Metric struct {
	Name      string
	Timestamp time.Time
	State     pkg.Attribute
}

// GetMetric func - returning metric, if Metric is nil, so nothing in DB.
func GetMetric(name string) (*Metric, error) {
	tnx := db.Txn(false)
	defer tnx.Abort() // noop for read transaction

	obj, err := tnx.First(tblState, idx, name)
	if err != nil {
		return nil, err
	}

	switch v := obj.(type) {
	case *Metric:
		return v, nil
	case nil:
		return &Metric{}, nil
	default:
		return nil, fmt.Errorf("in-mem db, wrong type of saved data: %T", obj)
	}
}

// PutMetric func - inserting or updating in db metric by name.
func PutMetric(data *Metric) (err error) {
	tnx := db.Txn(true)
	defer tnx.Commit()

	// Put data into in-mem db
	if err = tnx.Insert(tblState, data); err != nil {
		return fmt.Errorf("in-mem db, can't write data: %v", err)
	}

	// FixMe, required refactoring
	if w != nil {
		log.Debugf("Sending %+s into influx db channel", data)

		batchPoint <- data
	}

	return nil
}
