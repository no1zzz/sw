package model

import (
	"os"
	"sync"
	"time"

	"github.com/influxdata/influxdb/client/v2"
	"github.com/safervpn/saferwatcher/pkg/backends/influxdb"
)

var (
	batchPoint chan *Metric
	w          *daemon
)

const (
	precision       = "s"
	retentionPolicy = "autogen"
)

func InitInfluxDB(dbName string, maxQueue int, pollInterval time.Duration) error {
	var err error

	log.Debug("Initializing internal InfluxDB worker")
	w = newDaemon(dbName, pollInterval)
	if err = w.run(maxQueue); err != nil {
		return err
	}
	log.Debug("Internal InfluxDB worker initialized")

	return nil
}

func Shutdown() {
	defer w.stop()
}

type daemon struct {
	dbName       string
	hostname     string
	pollInterval time.Duration
	quit         chan bool
	sync.RWMutex
}

func newDaemon(db string, pollInterval time.Duration) *daemon {
	hostname, err := os.Hostname()
	if err != nil {
		log.Critf("Can't get hostname: %v", err)
	}

	return &daemon{
		dbName:       db,
		hostname:     hostname,
		pollInterval: pollInterval,
		quit:         make(chan bool),
	}
}

func (d *daemon) run(maxQueue int) error {
	d.Lock()
	defer d.Unlock()

	go func() {
		// Init main queue for workers.
		batchPoint = make(chan *Metric, maxQueue)

		for {
			select {
			case <-d.quit:
				// we have received a signal to stop
				log.Debug("Shutdown internal worker")

				return
			case <-time.After(d.pollInterval):
				log.Debug("InfluxDB worker starting new job's cycle, for data points")

				var (
					bp  client.BatchPoints
					pt  *client.Point
					err error
				)

				bp, err = client.NewBatchPoints(
					client.BatchPointsConfig{
						Database:        d.dbName,
						Precision:       precision,
						RetentionPolicy: retentionPolicy,
					},
				)
				if err != nil {
					log.Err(err)
				}

				for i := 0; i <= len(batchPoint); i++ {
					point := <-batchPoint
					log.Debugf("Dispatching point: %s", point)

					tags := map[string]string{
						"name": point.Name,
						"host": d.hostname,
					}

					pt, err = client.NewPoint(point.Name, tags, point.State, point.Timestamp)
					if err != nil {
						log.Errf("InfluxDB worker, can't create data point: %v", err)
					}

					bp.AddPoint(pt)

					log.Debugf("Point dispatched: %s", point)
				}

				c, err := influxdb.NewClient()
				if err != nil {
					log.Errf("InfluxDB worker, can't get new db client: %v", err)
				}

				if err = c.Write(bp); err != nil {
					log.Errf("InfluxDB worker, can't write data points into db: %v", err)
				}

				log.Debug("InfluxDB worker job's cycle done, for data points")
			}
		}
	}()

	return nil
}

func (d *daemon) stop() {
	d.Lock()
	defer d.Unlock()

	d.quit <- true
}
