FROM golang:1.10

MAINTAINER anton@safervpn.com

# ENV
ENV NAME="saferwatcher"
ENV SHELL="/usr/bin/env bash"
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/go/bin:/go/bin:$PATH"
ENV WORKDIR="/go/src/github.com/safervpn/saferwatcher"
ENV GOPATH="/go"

WORKDIR $WORKDIR

# Copying config
COPY . $WORKDIR

# Installing dep
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# Building saferwatcher
RUN make dep build
